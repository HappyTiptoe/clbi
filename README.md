<div align="center">
  <a href="https://gitlab.com/happytiptoe/clbi">
    <img src="https://gitlab.com/HappyTiptoe/clbi/-/raw/main/assets/logo.png" alt="@happytiptoe/clbi - opinionated Vue project scaffolder" width="251">
  <br>
  </a>
  <h1>@happytiptoe/clbi</h1>
  <h3>✨ Opinionated Vue project scaffolder ✨</h3>
</div>

<div align="center">
<a href="https://www.npmjs.com/package/@happytiptoe/clbi" target="__blank">
  <img src="https://img.shields.io/npm/v/@happytiptoe/clbi?color=a4de52&label=" alt="NPM version">
</a>
<a href="https://gitlab.com/happytiptoe/clbi" target="__blank">
  <img src="https://img.shields.io/static/v1?label=&message=View%20documentation&color=a4de52" alt="view documentation">
</a>
<br>
<a href="https://gitlab.com/happytiptoe/tsu" target="__blank">
  <img src="https://img.shields.io/gitlab/stars/happytiptoe/tsu?style=flat&color=ffffde" alt="gitlab stars">
</a>
<a href="https://npmjs.com/package/@happytiptoe/clbi">
  <img src="https://img.shields.io/npm/l/@happytiptoe/clbi?color=ffffde" alt="license">
</a>
</div>

## Features

- 🎨 Atomic CSS ([Tailwind CSS](https://tailwindcss.com/))
- 📦 Component Auto-Importing ([unplugin-vue-components](https://github.com/antfu/unplugin-vue-components/))
- 📄 File-Based Routing ([vite-plugin-pages](https://github.com/hannoeru/vite-plugin-pages/))
- 🍍 Global State Management ([Pinia](https://pinia.vuejs.org/))
- 👌 Icons from any Icon Sets ([Iconify](https://iconify.design/docs/icon-components/vue/))
- 📷 Static-Site Generation (SSG) ([vite-ssg](https://github.com/antfu-collective/vite-ssg/))
- 🚀 [TypeScript](https://www.typescriptlang.org/)
- 🤝 TypeScript Utilities ([tsu](https://gitlab.com/happytiptoe/tsu/))
- ⚡️ [Vite](https://vitejs.dev/)
- 🖖 [Vue](https://vuejs.org/)
- 🛠️ [VueUse](https://vueuse.org/)
- 🔎 [Vue Dev Tools](https://devtools.vuejs.org/)
- 🗺️ [Vue Router](https://router.vuejs.org/)
- 📥 Vue API Auto-Importing ([unplugin-auto-import](https://github.com/antfu/unplugin-auto-import/))

## Installation

Install globally with your favourite package manager:

```sh
# yarn
yarn global add @happytiptoe/clbi

# pnpm
pnpm add --global @happytiptoe/clbi

# npm
npm install --global @happytiptoe/clbi
```

Initialise a new project:

```sh
clbi <project-name>
```

## License

[MIT](http://opensource.org/licenses/MIT)
