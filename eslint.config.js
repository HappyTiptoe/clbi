import eslint from '@eslint/js'
import tsEslint from 'typescript-eslint'
import eslintConfigPrettier from 'eslint-config-prettier'

export default tsEslint.config({
  files: ['**/*.js', '**/*.mjs', '**/*.ts'],
  ignores: ['**/dist/**', '**/bin/**'],
  extends: [eslint.configs.recommended, ...tsEslint.configs.recommended, eslintConfigPrettier],
  rules: {
    '@typescript-eslint/ban-ts-comment': 'off',
    '@typescript-eslint/no-non-null-assertion': 'off',
    '@typescript-eslint/no-unused-vars': 'off',
    'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'no-undef': 'off',
    'no-unused-vars': 'off',
    'prefer-const': 'off'
  }
})
