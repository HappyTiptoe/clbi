import { defineConfig } from 'tsup'

export default defineConfig({
  entry: { clbi: 'src/index.ts' },
  outDir: 'bin',
  format: ['esm'],
  dts: false,
  clean: true,
  minify: true
})
