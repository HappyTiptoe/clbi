import eslint from '@eslint/js'
import eslintConfigPrettier from 'eslint-config-prettier'
import pluginVue from 'eslint-plugin-vue'
import tsEslint from 'typescript-eslint'

export default tsEslint.config({
  files: ['**/*.js', '**/*.mjs', '**/*.ts', '**/*.jsx', '**/*.vue'],
  ignores: ['**/dist/**'],

  extends: [
    eslint.configs.recommended,
    ...tsEslint.configs.recommended,
    ...pluginVue.configs['flat/recommended'],
    eslintConfigPrettier
  ],

  languageOptions: {
    parserOptions: {
      parser: tsEslint.parser,
      extraFileExtensions: ['.vue'],
      sourceType: 'module'
    }
  },

  rules: {
    '@typescript-eslint/ban-ts-comment': 'off',
    '@typescript-eslint/no-non-null-assertion': 'off',
    '@typescript-eslint/no-unused-vars': 'off',
    'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'no-undef': 'off',
    'no-unused-vars': 'off',
    'prefer-const': 'off',
    'vue/multi-word-component-names': 'off',
    'vue/no-dupe-keys': 'off'
  }
})
