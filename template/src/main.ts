import { persistia } from 'persistia'
import { createPinia } from 'pinia'
import { ViteSSG } from 'vite-ssg'
import routes from 'virtual:generated-pages'
import App from './App.vue'

import '@fontsource-variable/inter'
import './css/global.css'

export const createApp = ViteSSG(App, { routes }, ({ app }) => {
  const pinia = createPinia()
  pinia.use(persistia)

  app.use(pinia)
})
