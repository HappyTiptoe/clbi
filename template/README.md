# @HappyTiptoe/clbi Project

This template is intended to kickstart your development with [Vue 3](https://vuejs.org/) and [Vite](https://vitejs.dev/). It follows an opinionated project structure, not too dissimilar to that of [Nuxt](https://nuxt.com/)!

## Features

- 🎨 Atomic CSS ([Tailwind CSS](https://tailwindcss.com/))
- 📦 Component Auto-Importing ([unplugin-vue-components](https://github.com/antfu/unplugin-vue-components/))
- 📄 File-Based Routing ([vite-plugin-pages](https://github.com/hannoeru/vite-plugin-pages/))
- 🍍 Global State Management ([Pinia](https://pinia.vuejs.org/))
- 👌 Icons from any Icon Sets ([Iconify](https://iconify.design/docs/icon-components/vue/))
- 📷 Static-Site Generation (SSG) ([vite-ssg](https://github.com/antfu-collective/vite-ssg/))
- 🚀 [TypeScript](https://www.typescriptlang.org/)
- 🤝 TypeScript Utilities ([tsu](https://gitlab.com/happytiptoe/tsu/))
- ⚡️ [Vite](https://vitejs.dev/)
- 🖖 [Vue](https://vuejs.org/)
- 🛠️ [VueUse](https://vueuse.org/)
- 🔎 [Vue Dev Tools](https://devtools.vuejs.org/)
- 🗺️ [Vue Router](https://router.vuejs.org/)
- 📥 Vue API Auto-Importing ([unplugin-auto-import](https://github.com/antfu/unplugin-auto-import/))

## Setup Project

### Install dependencies

```sh
# yarn
yarn install

# pnpm
pnpm install

# npm
npm install
```

### Launch the development server:

Start the development server on `http://localhost:8888`:

```sh
# yarn
yarn dev

# pnpm
pnpm run dev

# npm
npm run dev
```

Or use a custom port:

```sh
# yarn
yarn dev --port 1234

# pnpm
pnpm run dev --port 1234

# npm
npm run dev --port 1234
```

### Build the application for production:

```sh
# yarn
yarn build

# pnpm
pnpm run build

# npm
npm run build
```

### Preview the production build locally:

```sh
# yarn
yarn preview

# pnpm
pnpm run preview

# npm
npm run preview
```

## Customise Configuration

See [Vite Configuration Reference](https://vitejs.dev/config/)
