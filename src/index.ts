#!/usr/bin/env node

import fse from 'fs-extra'
import { green, white } from 'kolorist'
import mri from 'mri'
import path from 'node:path'
import { fileURLToPath } from 'node:url'
import { error, isEmptyDir, nameToDir } from './utils'
import { version } from '../package.json'

const argv = mri(process.argv.slice(2))
console.log(`@happytiptoe/${green('clbi')} (${version})`)

async function run() {
  const name = argv._[0]

  if (argv.v || argv.version) {
    return
  }

  if (!name) {
    error("Oops, looks like you didn't enter a project name!")
  }

  const dir = nameToDir(name)

  if (!isEmptyDir(dir)) {
    error(`Oops, looks like ${white(name)} isn't empty!`)
  }

  const templateDir = path.resolve(fileURLToPath(import.meta.url), '../../template')

  // copy template to target
  await fse.copy(templateDir, dir)

  await fse.rename(`${dir}/_gitignore`, `${dir}/.gitignore`)
  await fse.rename(`${dir}/_eslint.config.js`, `${dir}/eslint.config.js`)

  console.log(`🌱 Clbi project created! Next steps:`)
  console.log(` › ${green(`cd ${name}`)}`)
  // prettier-ignore
  console.log(` › Install dependencies with ${green('yarn install')} or ${green('npm install')} or ${green('pnpm install')}`)
  // prettier-ignore
  console.log(` › Start development server with ${green('yarn dev')} or ${green('npm run dev')} or ${green('pnpm run dev')}`)
}

run().catch((e) => {
  console.error(e.message)
})
