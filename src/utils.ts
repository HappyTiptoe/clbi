import fse from 'fs-extra'
import { red } from 'kolorist'

/* fs */

// prepends "./" to project path where appropriate
export function nameToDir(name: string) {
  if (name === '.') {
    return name
  }

  return name.startsWith('./') ? name : `./${name}`
}

// checks for empty directories
export function isEmptyDir(dirPath: string) {
  if (!fse.existsSync(dirPath)) {
    return true
  }

  const files = fse.readdirSync(dirPath)
  return files.length === 0
}

/* io */

// pretty error messages
export function error(msg: string) {
  throw new Error(red(`✖ ${msg}`))
}
